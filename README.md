# Bad API Assignment

## Assignment

Assignment brief for junior developers

> Your client is a clothing brand that is looking for a simple web app to use in their warehouses. To do their work efficiently, the warehouse workers need a fast and simple listing page per product category, where they can check simple product and availability information from a single UI. There are three product categories they are interested in for now: gloves, facemasks, and beanies. Therefore, you should implement three web pages corresponding to those categories that list all the products in a given category. One requirement is to be easily able to switch between product categories quickly. You are free to implement any UI you want, as long as it can display multiple products at once in a straightforward and explicit listing. At this point, it is not necessary to implement filtering or pagination functionality on the page.

> The client does not have a ready-made API for this purpose. Instead, they have two different legacy APIs that combined can provide the needed information. The legacy APIs are on critical-maintenance-only mode, and thus further changes to them are not possible. The client knows the APIs are not excellent, but they are asking you to work around any issues in the APIs in your new application. The client has instructed you that both APIs have an internal cache of about 5 minutes.

## Solution

Hosted on Azure App Services https://badapiproxy.azurewebsites.net

The purpose of Bad API Proxy is to handle original "Bad API" responses and provide "Better API" responses to UI in most convenient format to consume and therefore to improve DX / Frontend developers work and with functionalities such as cache and Websocket eventually UI / UX.

### Done

- Single GET interfaces to retrieve stock (which combines responses from original interfaces). This interfaces handle all errors and inconsistencies which may arise from the original API interfaces and transforms response to "better" API
- In-memory cache (Redis "too expensive" on Azure as solution requires live deployment) which caches all responses for one minute (reduces response time from seconds to milliseconds) and restricts loads on original legacy API
- OpenAPI 3.0 specs about API with Swagger UI (https://badapiproxy.azurewebsites.net/api/docs/)
- Custom logger implementation with "fastest kid in town" Pino (see https://github.com/pinojs/pino/blob/master/docs/benchmarks.md)
- Websocket with Cron task scheduler to retrieve every 5 minutes possible updates (as there was ~5 minute internal cache in legacy API) which is meant to improve usability so that warehouse workers does not need to manually refresh warehouse stock from slow "bad" API. Reduces requests to original legacy API
- Custom pipe to validate queries in order to make interface "bulletproof"
- GitLab CI pipeline to Azure Container Registry (CD pipeline not made though not difficult with Azure webhooks between resources)
- Custom CLI with Makefile to automate Docker commands (has also Docker compose file for local deployment)

### Learnings

- RxJS was not that familiar so was relatively happy how the syntax is able to handle all errors, retrying requests etc.
- First time implementing Websocket though NestJS Framework makes this easy

### Improvements

- TypeScript generics require some more learning as it took unnecessarily long to implement custom validation for interface queries which have enum type
- Most likely overkill solution to problem at hand
- There are no tests
- API does not conform to any official specification such as JSON:API (see https://jsonapi.org/)
- Folder structure and paths do not indicate API versioning
- Was a bit lazy here with the TSConfig ex. strict and paths
- There is no separate healthcheck (besides Docker container healthcheck which pings basepath)

## Installation

### Scripts

To run application on local machine in development mode

    npm run start

To run application on local machine in watch mode

    npm run start:dev

To run application on local machine in production mode

    npm run start:prod

### Makefile

To list available Makefile automation scripts and CLIs

    make help

To run application Docker automation scripts

    make docker

### Docker

To [create](https://docs.docker.com/engine/reference/commandline/image_build/) Docker image

    docker build -t <image-name>:<tag> .

To [list](https://docs.docker.com/engine/reference/commandline/images/) available Docker image(s)

    docker images

To [run](https://docs.docker.com/engine/reference/run/) Docker container

    docker run -d -p <localhost-port>:<container-port> <image-name>:<tag>

To [list](https://docs.docker.com/engine/reference/commandline/ps/) running Docker container(s)

    docker ps

To [stop](https://docs.docker.com/engine/reference/commandline/stop/) running Docker container(s)

    docker stop <container-id>

To [remove](https://docs.docker.com/engine/reference/commandline/image_rm/) Docker image(s)

    docker image rm <image-id>
