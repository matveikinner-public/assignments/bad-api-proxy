import { Module } from '@nestjs/common';
import { HttpModule } from 'src/shared/http/http.module';
import { WarehouseController } from './warehouse.controller';
import { WarehouseGateway } from './warehouse.gateway';
import { WarehouseService } from './warehouse.service';

@Module({
  imports: [HttpModule],
  controllers: [WarehouseController],
  providers: [WarehouseService, WarehouseGateway],
})
export class WarehouseModule {}
