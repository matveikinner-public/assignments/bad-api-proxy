import { Cron, CronExpression } from '@nestjs/schedule';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { WarehouseService } from './warehouse.service';

@WebSocketGateway()
export class WarehouseGateway {
  constructor(private readonly warehouseService: WarehouseService) {}

  @WebSocketServer()
  server: Server;

  @Cron(CronExpression.EVERY_5_MINUTES, {
    name: 'stock',
  })
  emitStock() {
    const event = 'stock';
    return this.warehouseService.getStock().subscribe((stock) => {
      const schedule = this.warehouseService.getSchedule();
      this.server.emit(event, { statusCode: 200, data: { schedule, stock } });
    });
  }
}
