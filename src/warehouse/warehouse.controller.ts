import { BadRequestException, Controller, Get, Query } from '@nestjs/common';
import { ApiBadRequestResponse, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import ParseQueryEnumArrayPipe from 'src/shared/pipes/parseQueryEnumArray.pipe';
import ProductTypeEnum from './enums/productType.enum';
import ApiBadQueryRequestResponse from './models/apiBadQueryRequestResponse.model';
import Schedule from './models/schedule.model';
import ScheduleResponse from './models/scheduleResponse.model';
import StockItem from './models/stockItem.model';
import StockItemResponse from './models/stockItemResponse.model';
import { WarehouseService } from './warehouse.service';

@ApiTags('Warehouse')
@Controller('warehouse')
export class WarehouseController {
  constructor(private readonly warehouseService: WarehouseService) {}

  @ApiResponse({ type: StockItemResponse, status: 200, description: 'Return stock item list.' })
  @ApiBadRequestResponse({ type: ApiBadQueryRequestResponse, status: 400 })
  @ApiQuery({ enum: ProductTypeEnum, name: 'types', isArray: true })
  @Get('stock')
  getStock(
    @Query('types', new ParseQueryEnumArrayPipe({ enum: ProductTypeEnum, optional: true }))
    query: ProductTypeEnum[]
  ): Observable<StockItem[]> {
    return this.warehouseService.getStock(query);
  }

  @ApiResponse({ type: ScheduleResponse, status: 200, description: 'Return warehouse Websocket Cron schedule' })
  @ApiBadRequestResponse({ type: BadRequestException })
  @Get('schedule')
  getSchedule(): Schedule {
    return this.warehouseService.getSchedule();
  }
}
