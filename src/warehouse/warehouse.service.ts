import { HttpService, Injectable } from '@nestjs/common';
import { forkJoin, Observable } from 'rxjs';
import { map, mergeMap, retryWhen, tap, withLatestFrom } from 'rxjs/operators';
import BadApiAvailabilityResponse from './models/badApiAvailabilityResponse.model';
import StockItemAvailability from './models/stockItemAvailability.model';
import StockItem from './models/stockItem.model';
import Product from './models/badApiProductResponse.model';
import ProductTypeEnum from './enums/productType.enum';
import { BAD_API_URL } from './warehouse.namespace';
import { LoggerService } from 'src/shared/logger/logger.service';
import { SchedulerRegistry } from '@nestjs/schedule';
import Schedule from './models/schedule.model';

@Injectable()
export class WarehouseService {
  constructor(
    private readonly logger: LoggerService,
    private readonly httpService: HttpService,
    private schedulerRegistry: SchedulerRegistry
  ) {
    this.logger.setContext(WarehouseService.name);
  }

  getProducts(query: ProductTypeEnum[] = Object.values(ProductTypeEnum)): Observable<Product[]> {
    const requests = query.map((type) =>
      this.httpService.get<Product[]>(`${BAD_API_URL.GET.PRODUCTS}/${type}`).pipe(map((res) => res.data))
    );

    return forkJoin(requests).pipe(map((products) => products.reduce((acc, cur) => [...acc, ...cur], [])));
  }

  getAvailability(query: string[]): Observable<StockItemAvailability[]> {
    const requests = query.map((manufacturer) =>
      this.httpService.get<BadApiAvailabilityResponse>(`${BAD_API_URL.GET.AVAILABILITY}/${manufacturer}`).pipe(
        map(({ data }) => {
          // Check for intentional built-in error case where "response": "[]"
          if (!Array.isArray(data.response)) {
            // Throw error as RxJS retryWhen expects error
            throw new Error(`Error while attempting to GET availability for manufacturer ${manufacturer}`);
          }
          return data.response.map((item) => ({
            id: item.id.toLowerCase(),
            availability: /<INSTOCKVALUE>(.*?)<\/INSTOCKVALUE>/g.exec(item.DATAPAYLOAD)[1].toLowerCase(),
            code: /<CODE>(.*?)<\/CODE>/g.exec(item.DATAPAYLOAD)[1],
          }));
        }),
        // Will retry HTTP calls indefinitely (assumes there will eventually always be a proper response)
        retryWhen((error) =>
          error.pipe(
            tap(() => this.logger.log(`Attempting to retry request for manufacturer ${manufacturer}`))
            // We can specify with ex. take(2) how many requests to retry in total
          )
        )
      )
    );

    return forkJoin(requests).pipe(map((availability) => availability.reduce((acc, cur) => [...acc, ...cur], [])));
  }

  getStock(query: ProductTypeEnum[] = Object.values(ProductTypeEnum)): Observable<StockItem[]> {
    const products = this.getProducts(query);

    return products.pipe(
      mergeMap((products) =>
        this.getAvailability(
          products
            .filter((value, index, self) => self.findIndex((m) => m.manufacturer === value.manufacturer) === index)
            .map((unique) => unique.manufacturer)
        )
      ),
      withLatestFrom(products),
      map(([availability, products]) =>
        products.map((product) => ({ ...product, ...availability.find((value) => value.id === product.id) }))
      )
    );
  }

  getSchedule(): Schedule {
    const job = this.schedulerRegistry.getCronJob('stock');

    return {
      isActive: job.running,
      lastUpdate: job.lastDate()?.getTime() || null,
      nextUpdate: job.nextDate().toDate().getTime(),
    };
  }
}
