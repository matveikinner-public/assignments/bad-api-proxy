enum ProductTypeEnum {
  GLOVES = 'gloves',
  BEANIES = 'beanies',
  FACEMASKS = 'facemasks',
}

export default ProductTypeEnum;
