import { ApiProperty } from '@nestjs/swagger';

export default class Schedule {
  @ApiProperty({ type: Boolean, example: true })
  isActive: boolean;
  @ApiProperty({ type: Number, example: 1614428400010 })
  lastUpdate: number | null;
  @ApiProperty({ type: Number, example: 1614428700000 })
  nextUpdate: number;
}
