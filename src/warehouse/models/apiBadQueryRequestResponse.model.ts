import { ApiProperty } from '@nestjs/swagger';

export default class ApiBadQueryRequestResponse {
  @ApiProperty({ type: Number, example: 400 })
  statusCode: number;

  @ApiProperty({ type: String, example: 'Validation failed (parsable array expected)' })
  message: string;

  @ApiProperty({ type: String, example: 'Bad Request' })
  error: string;
}
