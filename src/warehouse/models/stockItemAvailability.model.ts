import { ApiProperty } from '@nestjs/swagger';

export default class StockItemAvailability {
  @ApiProperty({ type: String, example: '22538e11b9fe7a91098' })
  id: string;

  @ApiProperty({ type: String, example: 'instock' })
  availability: string;

  @ApiProperty({ type: String, example: '200' })
  code: string;
}
