import { ApiProperty } from '@nestjs/swagger';
import ProductTypeEnum from '../enums/productType.enum';

export default class StockItem {
  @ApiProperty({ type: String, example: 'fc9d9c9faaf297444a0d9' })
  id: string;

  @ApiProperty({ type: String, example: 'instock' })
  availability: string;

  @ApiProperty({ type: String, example: '200' })
  code: string;

  @ApiProperty({ enum: ProductTypeEnum })
  type: string;

  @ApiProperty({ type: String, example: 'GINVE MAGIC' })
  name: string;

  @ApiProperty({ type: String, isArray: true, example: ['white'] })
  color: string[];

  @ApiProperty({ type: Number, example: 29 })
  price: number;

  @ApiProperty({ type: String, example: 'umpante' })
  manufacturer: string;
}
