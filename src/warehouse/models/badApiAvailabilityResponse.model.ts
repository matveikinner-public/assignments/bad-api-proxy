import { ApiProperty } from '@nestjs/swagger';

class BadApiAvailability {
  @ApiProperty({ type: String, example: 'D9C7EEF4C54B415A7B4B231A' })
  id: string;

  @ApiProperty({
    type: String,
    example: '<AVAILABILITY>\n  <CODE>200</CODE>\n  <INSTOCKVALUE>INSTOCK</INSTOCKVALUE>\n</AVAILABILITY>',
  })
  DATAPAYLOAD: string;
}

export default class BadApiAvailabilityResponse {
  @ApiProperty({ type: Number, example: 200 })
  code: number;

  @ApiProperty({ type: BadApiAvailability, isArray: true })
  response: BadApiAvailability[];
}
