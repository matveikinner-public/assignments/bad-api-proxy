import { ApiProperty } from '@nestjs/swagger';
import BadApiProductResponse from './badApiProductResponse.model';

export default class BetterApiProductResponse {
  @ApiProperty({ type: Number, example: 200 })
  statusCode: number;

  @ApiProperty({ type: BadApiProductResponse, isArray: true })
  data: BadApiProductResponse[];
}
