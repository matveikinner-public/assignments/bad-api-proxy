import { ApiProperty } from '@nestjs/swagger';
import StockItem from './stockItem.model';

export default class StockItemResponse {
  @ApiProperty({ type: Number, example: 200 })
  statusCode: number;

  @ApiProperty({ type: StockItem, isArray: true })
  data: StockItem[];
}
