import { ApiProperty } from '@nestjs/swagger';
import ProductTypeEnum from '../enums/productType.enum';

export default class BadApiProductResponse {
  @ApiProperty({ type: String, example: 'd9c7eef4c54b415a7b4b231a' })
  id: string;

  @ApiProperty({ enum: ProductTypeEnum, example: 'gloves' })
  type: string;

  @ApiProperty({ type: String, example: 'VEUPTAI JUMP LIGHT' })
  name: string;

  @ApiProperty({ type: String, isArray: true, example: ['green'] })
  color: string[];

  @ApiProperty({ type: Number, example: 86 })
  price: number;

  @ApiProperty({ type: String, example: 'juuran' })
  manufacturer: string;
}
