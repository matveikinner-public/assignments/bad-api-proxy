import { ApiProperty } from '@nestjs/swagger';
import StockItemAvailability from './stockItemAvailability.model';

export default class StockItemAvailabilityResponse {
  @ApiProperty({ type: Number, example: 200 })
  statusCode: number;

  @ApiProperty({ type: StockItemAvailability, isArray: true })
  data: StockItemAvailability[];
}
