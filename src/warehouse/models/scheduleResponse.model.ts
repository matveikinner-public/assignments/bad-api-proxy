import { ApiProperty } from '@nestjs/swagger';
import Schedule from './schedule.model';

export default class ScheduleResponse {
  @ApiProperty({ type: Number, example: 200 })
  statusCode: number;

  @ApiProperty({ type: Schedule })
  data: Schedule;
}
