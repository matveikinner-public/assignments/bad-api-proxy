export namespace BAD_API_URL {
  export enum GET {
    PRODUCTS = 'v2/products',
    AVAILABILITY = 'v2/availability',
  }
}
