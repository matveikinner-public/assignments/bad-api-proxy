import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ConfigEnum } from './config/config.enum';
import { LoggerService } from './shared/logger/logger.service';

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule, {
    logger: new LoggerService(),
  });

  const configService = app.get(ConfigService);
  const port = configService.get<number>(`${ConfigEnum.DEFAULT}.port`);

  // https://github.com/nestjs/nest/pull/5291#issuecomment-771601789
  app.setGlobalPrefix('api');

  app.enableCors();

  const config = new DocumentBuilder()
    .setTitle('Bad API')
    .setDescription('Bad API Assignment')
    .setVersion('0.0.1')
    .setBasePath('api')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api/docs', app, document);

  await app.listen(port);
};

void bootstrap();
