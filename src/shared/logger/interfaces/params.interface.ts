import { LoggerOptions } from 'pino';
import { RouteInfo } from '@nestjs/common/interfaces';

interface Params {
  options?: LoggerOptions;
  forRoutes?: (string | RouteInfo)[];
}

export default Params;
