import { Abstract, ModuleMetadata, Type } from '@nestjs/common';
import Params from './params.interface';

interface AsyncParams extends Pick<ModuleMetadata, 'imports' | 'providers'> {
  useFactory: (...args: any[]) => Params | Promise<Params>;
  inject?: (string | symbol | (() => any) | Type<any> | Abstract<any>)[];
}

export default AsyncParams;
