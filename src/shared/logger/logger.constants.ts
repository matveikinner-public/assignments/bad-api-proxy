import { RequestMethod } from '@nestjs/common';

export const PINO_LOGGER_OPTIONS = 'PINO_LOGGER_OPTIONS';
export const DEFAULT_ROUTES = [{ path: '*', method: RequestMethod.ALL }];
