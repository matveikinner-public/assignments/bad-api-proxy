import { BadRequestException } from '@nestjs/common';
import { PipeTransform } from '@nestjs/common';

const VALIDATION_ERROR_MESSAGE = 'Validation failed (parsable array expected)';
const VALIDATION_TYPE_ERROR_MESSAGE = 'Validation failed (unexpected value)';
const DEFAULT_ARRAY_SEPARATOR = ',';

export interface ParseQueryEnumArrayOptions<T> {
  enum: T;
  separator?: string;
  optional?: boolean;
}

export default class ParseQueryEnumArrayPipe<T> implements PipeTransform<T, Array<keyof T>> {
  constructor(private readonly options: ParseQueryEnumArrayOptions<T>) {}

  transform(value: any): Array<keyof T> {
    if (!value && !this.options.optional) {
      throw new BadRequestException(VALIDATION_ERROR_MESSAGE);
    } else if (!value && this.options.optional) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return value;
    }

    const queryItems = this.parseArray(value);

    return queryItems.map((queryItem) => {
      if (!Object.values(this.options.enum).includes(queryItem)) {
        throw new BadRequestException(VALIDATION_TYPE_ERROR_MESSAGE);
      }
      // This is incorrect as well as return type of Array<keyof T> as return type is value of T i.e. string[]
      return this.options.enum[queryItem.toUpperCase()] as keyof T;
    });
  }

  parseArray(value: any) {
    if (!Array.isArray(value)) {
      if (typeof value !== 'string') {
        throw new BadRequestException(VALIDATION_ERROR_MESSAGE);
      } else {
        try {
          return value.trim().split(this.options.separator || DEFAULT_ARRAY_SEPARATOR);
        } catch {
          throw new BadRequestException(VALIDATION_ERROR_MESSAGE);
        }
      }
    }
  }
}
