import { HttpModule as BaseHttpModule, HttpService, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AxiosInstance } from 'axios';
import { ConfigEnum } from 'src/config/config.enum';

@Module({
  imports: [
    BaseHttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => await configService.get(ConfigEnum.HTTP),
      inject: [ConfigService],
    }),
  ],
  exports: [BaseHttpModule],
})
export class HttpModule {
  private client: AxiosInstance;

  constructor(private readonly httpService: HttpService) {
    this.client = this.httpService.axiosRef;
  }
}
