import { registerAs } from '@nestjs/config';

export default registerAs('DEFAULT', () => ({
  port: parseInt(process.env.PORT) || 3000,
}));
