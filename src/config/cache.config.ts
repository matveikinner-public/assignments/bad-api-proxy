import { CacheManagerOptions } from '@nestjs/common';
import { registerAs } from '@nestjs/config';

/**
 * @see https://github.com/axios/axios#request-config
 */
export default registerAs(
  'CACHE',
  (): CacheManagerOptions => ({
    store: 'memory',
    ttl: 60, // Seconds
  })
);
