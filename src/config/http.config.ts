import { HttpModuleOptions } from '@nestjs/common';
import { registerAs } from '@nestjs/config';

/**
 * @see https://github.com/axios/axios#request-config
 */
export default registerAs(
  'HTTP',
  (): HttpModuleOptions => ({
    baseURL: process.env.BASE_URL || '',
    timeout: parseInt(process.env.HTTP_TIMEOUT) || 60000,
  })
);
