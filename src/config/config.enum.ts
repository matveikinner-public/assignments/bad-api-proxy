export enum ConfigEnum {
  HTTP = 'HTTP',
  LOGGER = 'LOGGER',
  CACHE = 'CACHE',
  DEFAULT = 'DEFAULT',
}
