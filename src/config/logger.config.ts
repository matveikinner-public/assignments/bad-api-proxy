import { registerAs } from '@nestjs/config';
import { LoggerOptions, SerializedRequest, SerializedResponse } from 'pino';

/**
 * @see https://getpino.io/#/docs/api?id=options
 */
export default registerAs(
  'LOGGER',
  (): LoggerOptions => ({
    serializers: {
      res: (res: SerializedResponse) => ({
        statusCode: res.statusCode,
      }),
      req: (req: SerializedRequest) => ({
        id: req.id,
        method: req.method,
        url: req.url,
        /**
         * Including the headers in the log could be in violation of privacy laws, e.g. GDPR. You should use the
         * "redact" option to remove sensitive fields. It could also leak authentication data in the logs.
         */
        headers: req.headers,
        port: req.remotePort,
      }),
    },
    timestamp: true,
    level: process.env.NODE_ENV === 'development' ? 'trace' : 'info',
  })
);
