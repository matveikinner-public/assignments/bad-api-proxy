import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import cacheConfig from './config/cache.config';
import { ConfigEnum } from './config/config.enum';
import defaultConfig from './config/default.config';
import httpConfig from './config/http.config';
import loggerConfig from './config/logger.config';
import { TransformInterceptor } from './shared/interceptors/transform.interceptor';
import { LoggerModule } from './shared/logger/logger.module';
import { WarehouseModule } from './warehouse/warehouse.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [httpConfig, cacheConfig, loggerConfig, defaultConfig],
      isGlobal: true,
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => await configService.get(ConfigEnum.LOGGER),
      inject: [ConfigService],
    }),
    CacheModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => await configService.get(ConfigEnum.CACHE),
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    WarehouseModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule {}
